//
//  IntroScene.swift
//  Hello Cocos2D Swift
//
//  Created by Daniel Zhang (張道博) on 7/27/14.
//  Copyright (c) 2014 Daniel Zhang (張道博). All rights reserved.
//

import Foundation

class IntroScene : CCScene {

    init() {
        super.init()

        let background = CCNodeColor.nodeWithColor(
            CCColor(red:0.2, green:0.2, blue:0.2, alpha:1.0)) as CCNodeColor
        self.addChild(background)

        let label = CCLabelTTF(string:"Hello Cocos2D-Swift World", fontName:"Chalkduster",
            fontSize:36)
        label.positionType = CCPositionType.Normalized
        label.color = CCColor.redColor()
        label.position = CGPointMake(0.5, 0.5)
        self.addChild(label)

        let helloWorldButton = CCButton(title:"[ Start ]", fontName:"Verdana-Bold",
            fontSize:18)
        helloWorldButton.positionType = CCPositionType.Normalized
        helloWorldButton.position = CGPointMake(0.5, 0.35)
        helloWorldButton.setTarget(self, selector:"onSpinningClicked:")
        self.addChild(helloWorldButton)
    }

    func onSpinningClicked(sender:AnyObject)
    {
        CCDirector.sharedDirector().replaceScene(HelloWorldScene(),
            withTransition: CCTransition(
                pushWithDirection: CCTransitionDirection.Left, duration: 1.0))
    }

    class func scene() -> CCScene {
        return IntroScene().scene
    }
}

extension CCPositionType {
    static var Normalized:CCPositionType {
        get { return CCPositionTypeMake(
            CCPositionUnit.Normalized,
            CCPositionUnit.Normalized,
            CCPositionReferenceCorner.BottomLeft) }
    }
}
