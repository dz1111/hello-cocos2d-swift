//
//  main.m
//  Hello Cocos2D Swift
//
//  Created by Daniel Zhang (張道博) on 7/27/14.
//  Copyright Daniel Zhang (張道博) 2014. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    @autoreleasepool {
        int retVal = UIApplicationMain(argc, argv, nil, @"AppDelegate");
        return retVal;
    }
}
